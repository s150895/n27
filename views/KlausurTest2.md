#Klausur / Test 2 am 14.01.2020 um 9:10 Uhr 

1. Fehler finden und auf Papier dokumentieren

2. Alles, was in T1 / K1 relevant war

3. Selbst etwas ausprogrammieren
(GUI oder in der server.js)

4. SQ. Eventuell einen unbekannten SQL-Befehl anahnd einer gegebenen Dokumentation erstellen.

5. if und else (auch verschachtelt)

Beispiel:
```Javascript
//Wenn ein Schüler / eine Schülerin nicht volljährig ist, wird "Eintritt verweigert".

var darfHinein = "nein"

var alter = 18;

if(alter >= 18){
    darfHinein = "ja"
}

Console.Log("Der Schüler / Die Schülerin darf hinein: " + darfHinein)

```


```Javascript
//Wenn ein Schüler / eine Schülerin nicht volljährig ist, wird "Eintritt verweigert".

var darfHinein = ""

var alter = 18;

if(alter >= 18){
    darfHinein = "ja"
} else{
    darfHinein = "nein"
}

Console.Log("Der Schüler / Die Schülerin darf hinein: " + darfHinein)

```
```Javascript
//Wenn ein Schüler / eine Schülerin nicht volljährig ist, wird "Eintritt verweigert".

// Schülerinnen zehlen 3 Euro.

// Schüler zahlen 4 Euro.

var darfHinein = "true"

var istVolljaehrig = true;

var geschlecht = "w"

if(istVolljaehrig){
    darfHinein = true
    Console.Log("Der Schüler / Die Schülerin darf hinein.")
} else{
    darfHinein = false
    Console.Log("Der Schüler / Die Schülerin darf nicht hinein.")
}

```

6. symmetrische und asymmetrische Verschlüsserung erklären / gegeneinander abgrenzen. Den Sinn jeweils erklären. Die Implementation am Rechner kur beschreiben.
